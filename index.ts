import Cacheable from 'cacheable-methods-dynamodb';

class TestClass {
  //one Day
  @Cacheable(1) //This decorator uses dynamoDb to cache data, in the exemple 1 day
  public async getUserById(): Promise<any> {
    return new Promise((resolve)=>{
      resolve({name: "andrade", prenom: "pauline"})
    });
  }
}

const function1 = async (_event, _context) => {
  try {
    const a = new TestClass();
    const response = await a.getUserById();
    return {
      statusCode: 200,
      body: JSON.stringify(response),
      headers: {
        'Content-Type': 'application/json',
      }
    };
  } catch (e) {
    const errorResponse = {
      statusCode: 200,
      body: JSON.stringify({
        message: e.message
      }),
    };
    return errorResponse;
  }
};

// @ts-ignore
export { function1 }
